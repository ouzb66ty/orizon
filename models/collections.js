/* Requires */

const jwt = require('jsonwebtoken');

/* Class */

module.exports = class Collections {

	constructor(app, db) {
		this.app = app;
		this.db = db;
	}

	create(method, uri, managePrivileges, resource) {
		if (method.toUpperCase() == 'GET') {
			this.app.get(uri, managePrivileges, (req, res) => {
	            jwt.verify(req.token, 'jwtRS256.key', (err, user) => {
	                resource(this.db, req, res, user);
	            });
	        });
		} else if (method.toUpperCase() == 'POST') {
			this.app.post(uri, managePrivileges, (req, res) => {
	            jwt.verify(req.token, 'jwtRS256.key', (err, user) => {
	                resource(this.db, req, res, user);
	            });
	        });
		} else if (method.toUpperCase() == 'PUT') {
			this.app.put(uri, managePrivileges, (req, res) => {
	            jwt.verify(req.token, 'jwtRS256.key', (err, user) => {
	                resource(this.db, req, res, user);
	            });
	        });
		} else if (method.toUpperCase() == 'DELETE') {
			this.app.delete(uri, managePrivileges, (req, res) => {
	            jwt.verify(req.token, 'jwtRS256.key', (err, user) => {
	                resource(this.db, req, res, user);
	            });
	        });
		} else {
			console.warn("[COLLECTIONS] Unknown method");
		}
	}

};