/* DB connection */

const { MongoClient } = require("mongodb");
const uri = 'mongodb://localhost/orizon';

(async function() {
    try {

        /* Requires */

        const client = await MongoClient.connect(uri,{ useNewUrlParser: true, useUnifiedTopology: true });
        const express = require('express');
        const cors = require('cors');
        const app = express();
        const bodyParser = require('body-parser');
        const auth = require('./resources/auth');
        const videos = require('./resources/videos');
        const collectionsModule = require('./models/collections');
        const fileUpload = require('express-fileupload');
        const db = await client.db();
        const collections = new collectionsModule(app, db);
        const port = 8001;

        /* Config */
        app.use(cors({
            origin: '*',
            optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
        }));
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(fileUpload({
            useTempFiles : true,
            tempFileDir : '/tmp/'
        }));

        /* Get Bearer token for auth */

        function managePrivileges(req, res, next) {

            const bearerHeader = req.headers["authorization"];

            if (typeof bearerHeader != 'undefined') {
                const bearer = bearerHeader.split(" ");
                const bearerToken = bearer[1];
                req.token = bearerToken;
                next();
            } else {
                res.statusCode = 401;
                res.json({
                    'error': 'Need correct API token in Bearer token'
                });
            }
        }

        /* Auth collections */

        collections.create("POST", '/orizon/v1/auth/login', (req, res, next) => { next(); }, auth.login);
        collections.create("POST", '/orizon/v1/auth/register', (req, res, next) => { next(); }, auth.register);

        collections.create("GET", '/orizon/v1/auth/connections', managePrivileges, auth.getConnections);
        collections.create("GET", '/orizon/v1/auth/myConnections', managePrivileges, auth.getMyConnections);
        collections.create("GET", '/orizon/v1/auth/users', managePrivileges, auth.getUsers);
        collections.create("GET", '/orizon/v1/auth/users/:username', managePrivileges, auth.getUser);
        collections.create("PUT", '/orizon/v1/auth/username/:username', managePrivileges, auth.putUsername);
        collections.create("PUT", '/orizon/v1/auth/mail/:mail', managePrivileges, auth.putMail);
        collections.create("PUT", '/orizon/v1/auth/password/:password', managePrivileges, auth.putPassword);
        collections.create("PUT", '/orizon/v1/auth/privileges/:username/:bool', managePrivileges, auth.putPrivileges);
        collections.create("DELETE", '/orizon/v1/auth/user/:username', managePrivileges, auth.deleteUser);

        /* Videos collections */

        collections.create("PUT", '/orizon/v1/videos/domain/:id', managePrivileges, videos.updateDomain);
        collections.create("PUT", '/orizon/v1/videos/description/:id', managePrivileges, videos.updateDescription);
        collections.create("POST", '/orizon/v1/videos', managePrivileges, videos.addVideo);
        collections.create("POST", '/orizon/v1/videos/export/:id', managePrivileges, videos.exportVideo);
        collections.create("GET", '/orizon/v1/videos', managePrivileges, videos.getVideos);
        collections.create("GET", '/orizon/v1/videos/username/:username', managePrivileges, videos.getUserVideos);
        collections.create("GET", '/orizon/v1/videos/page/:n', managePrivileges, videos.getNVideos);
        collections.create("GET", '/orizon/v1/videos/namespaces', managePrivileges, videos.getNamespacesVideo);
        collections.create("GET", '/orizon/v1/videos/id/:id', managePrivileges, videos.getVideo);
        collections.create("DELETE", '/orizon/v1/videos', managePrivileges, videos.deleteVideos);
        collections.create("DELETE", '/orizon/v1/videos/:id', managePrivileges, videos.deleteVideo);

        /* HTTP listening */

        app.listen(port);

    } catch(e) {

        /* DB error connection */

        console.error(e);
        
    }
    
})()
