$(document).ready(function(){

    /* Tools */

    function createCORSReq(method, url) {

        let req = new XMLHttpRequest();

        if ("withCredentials" in req) {
            req.open(method, url, true); 
        } else if (typeof XDomainRequest != "undefined") {
            req = new XDomainRequest();
            req.open(method, url, true); 
        } else {
            req.open(method, url, true); 
        }
        return (req);
    }

    /* Cookies */

    function getCookie(cname) {

        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    /* Events */

    var videoForm = document.getElementById('addVideoForm');

    videoForm.addEventListener('submit', function(e) {

        var formData = new FormData();
        var token = getCookie("token");
        var uploadField = document.getElementById("uploadVideo");
        var namespaceField = document.getElementById("namespaceVideo");
        var descriptionField = document.getElementById("descriptionVideo");
        var tagsField = document.getElementsByName("hidden-tags")[0];
        var request = createCORSReq('POST', 'http://127.0.0.1:8001/orizon/v1/videos');

        $("#waitModal").modal('show');
        if (uploadField.files && uploadField.files.length == 1) {
            var file = uploadField.files[0];

            formData.set("file", file, file.name);
        }
        formData.set("namespace", namespaceField.value);
        formData.set("description", descriptionField.value);
        formData.set("tags", tagsField.value);
        request.onreadystatechange = function () {
            if(request.readyState === 4 && request.status === 200) {
                $("#successModal").modal('show');
            }
        };
        request.setRequestHeader("Authorization", "Bearer " + token);
        request.send(formData);
        e.preventDefault();
        
    });
});