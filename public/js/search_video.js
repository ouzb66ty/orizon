/* Tools */

function createCORSReq(method, url) {

    let req = new XMLHttpRequest();

    if ("withCredentials" in req) {
        req.open(method, url, true); 
    } else if (typeof XDomainRequest != "undefined") {
        req = new XDomainRequest();
        req.open(method, url, true); 
    } else {
        req.open(method, url, true); 
    }
    return (req);
}

/* Cookies */

function getCookie(cname) {

    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

/* Filter search engine */

function filterSearch() {
    var input, filter, table, tr, td, i, txtValue;
    var domain = document.getElementById('domainFilter');
    var domainValue = domain.options[domain.selectedIndex].value;
    var hiddenTags = document.getElementsByName("hidden-tags")[0].value;
    var tags = hiddenTags.split(',');

    input = document.getElementById("inputFilter");
    filter = input.value.toUpperCase();
    table = document.getElementById("filterTable");
    tr = table.getElementsByTagName("tr");
    for (i = 1; i < tr.length; i++) {

        td = tr[i].getElementsByTagName("td")[0];
        domainTd = tr[i].getElementsByTagName("td")[1];
        tagsTd = tr[i].getElementsByTagName("td")[4].children;
        if (td) {
            var tagsIn = false;

            txtValue = td.textContent || td.innerText;
            domainTdValue = domainTd.textContent || domainTd.innerText;
            /* Tags */
            for (var y = 0; y < tagsTd.length; y++) {
                if (hiddenTags == '' || tags.includes(tagsTd[y].id) == true) {
                    tagsIn = true;
                }
            }
            /* Words */
            if (txtValue.toUpperCase().indexOf(filter) > -1
                && (domainValue == domainTdValue || domainValue == "*")
                && tagsIn == true) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

/* Trash buttons */

const trashVideos = document.getElementsByName("trash");
const trashVideosList = Array.prototype.slice.call(trashVideos);
const token = getCookie("token");

trashVideosList.forEach(function(video, index, list) {
    let card = video.parentElement;

    video.addEventListener("click", function(e) {
        let req = createCORSReq('DELETE', 'http://127.0.0.1:8001/orizon/v1/videos/' + card.id);

        req.onreadystatechange = function () {
            if(req.readyState === 4 && req.status === 200) {
                card.parentElement.style.display = 'none';
            }
        };
        req.setRequestHeader("Authorization", "Bearer " + token);
        req.send();
    });
});

/* Analyze */

const analyzeVideos = document.getElementsByName("analyze");
const analyzeVideosList = Array.prototype.slice.call(analyzeVideos);

analyzeVideosList.forEach(function(video, index, list) {
    let id = video.parentElement.id;

    video.addEventListener("click", function(e) {
        var request = createCORSReq('POST', 'http://127.0.0.1:8003/vision/v1/active_videos/');

        request.onreadystatechange = function () {
            if (request.readyState === 4) {
                if (request.status === 200)
                    $("#successModal").modal('show');
                else if (request.status === 449)
                    $("#waitModal").modal('show');
            }
        };
        request.setRequestHeader("Authorization", "Bearer " + token);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.send("id=" + id);
        e.preventDefault();
    });
});