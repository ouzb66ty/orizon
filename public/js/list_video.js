/* Tools */

function createCORSReq(method, url) {

    let req = new XMLHttpRequest();

    if ("withCredentials" in req) {
        req.open(method, url, true); 
    } else if (typeof XDomainRequest != "undefined") {
        req = new XDomainRequest();
        req.open(method, url, true); 
    } else {
        req.open(method, url, true); 
    }
    return (req);
}

/* Cookies */

function getCookie(cname) {

    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

/* Trash buttons */

const trashVideos = document.getElementsByName("trash");
const trashVideosList = Array.prototype.slice.call(trashVideos);
const token = getCookie("token");

trashVideosList.forEach(function(video, index, list) {
    let card = video.parentElement.parentElement;

    video.addEventListener("click", function(e) {
        let req = createCORSReq('DELETE', 'http://127.0.0.1:8001/orizon/v1/videos/' + card.id);

        req.onreadystatechange = function () {
            if(req.readyState === 4 && req.status === 200) {
                location.reload(true);
            }
        };
        req.setRequestHeader("Authorization", "Bearer " + token);
        req.send();
    });
});

/* Analyze */

const analyzeVideos = document.getElementsByName("analyze");
const analyzeVideosList = Array.prototype.slice.call(analyzeVideos);

analyzeVideosList.forEach(function(video, index, list) {
    let id = video.parentElement.parentElement.id;

    video.addEventListener("click", function(e) {
        var request = createCORSReq('POST', 'http://127.0.0.1:8003/vision/v1/active_videos/');

        request.onreadystatechange = function () {
            if (request.readyState === 4) {
                if (request.status === 200)
                    $("#successModal").modal('show');
                else if (request.status === 449)
                    $("#waitModal").modal('show');
            }
        };
        request.setRequestHeader("Authorization", "Bearer " + token);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.send("id=" + id);
        e.preventDefault();
    });
});